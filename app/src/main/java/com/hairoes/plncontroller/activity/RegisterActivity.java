package com.hairoes.plncontroller.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.hairoes.plncontroller.Config;
import com.hairoes.plncontroller.R;
import com.hairoes.plncontroller.model.RequestHandler;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    String nama, alamat, telp, user, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        EditText name = (EditText) findViewById(R.id.editTextName);
        EditText addr = (EditText) findViewById(R.id.editTextAlamat);
        EditText tlp = (EditText) findViewById(R.id.editTextTelp);
        EditText usr = (EditText) findViewById(R.id.editTextUserName);
        EditText pss = (EditText) findViewById(R.id.editTextPassword);

        nama = name.getText().toString();
        alamat = addr.getText().toString();
        telp = tlp.getText().toString();
        user = usr.getText().toString();
        pass = pss.getText().toString();
    }

    private void createAccount(View view) {

        class addUser extends AsyncTask<Void, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(RegisterActivity.this, "", "add user...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(RegisterActivity.this, s, Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String, String> params = new HashMap<>();
                params.put(Config.KEY_NAME, nama);
                params.put(Config.KEY_ADDR, alamat);
                params.put(Config.KEY_TELP, telp);
                params.put(Config.KEY_USER, user);
                params.put(Config.KEY_PASS, pass);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(Config.URL_ADD_ACCOUNT, params);
                return res;
            }
        }
        new addUser().execute();


    }
}
