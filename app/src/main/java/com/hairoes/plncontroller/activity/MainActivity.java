package com.hairoes.plncontroller.activity;

import android.app.AlertDialog;
import android.app.Application;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.hairoes.plncontroller.Config;
import com.hairoes.plncontroller.R;
import com.hairoes.plncontroller.model.ListData;
import com.hairoes.plncontroller.model.RecycleAdapter;
import com.hairoes.plncontroller.model.RequestHandler;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    final String TAG = this.getClass().getName();
    private Drawer.Result navigationDrawerLeft;
    private AccountHeader.Result headerNavigationLeft;
    private MaterialSearchView searchView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecycleAdapter adapter;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private Bundle savedInstanceState;


    private String user,nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Intent intent = getIntent();
        SharedPreferences pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        user = pref.getString("username", "");
        nama = pref.getString("nama", "");
//        user = intent.getExtras().getString("username");
//        nama = intent.getExtras().getString("nama");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddActivity.class);
                intent.putExtra("username", user);
                startActivity(intent);
            }
        });


        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.green, R.color.blue, R.color.pink);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetJSON();
            }
        });
        GetJSON();
        searc();
        navDrawer();

    }

    @Override
    protected void onResume() {
        super.onResume();
        GetJSON();
    }

    private void navDrawer() {
        Bundle bundle = getIntent().getExtras();
        headerNavigationLeft = new AccountHeader()
                .withActivity(this)
                .withCompactStyle(true)
                .withSavedInstance(savedInstanceState)
                .withThreeSmallProfileImages(true)
                .withHeaderBackground(R.drawable.custom_bg_button)
                .addProfiles(
                        new ProfileDrawerItem().withName(nama).withIcon(getResources().getDrawable(R.drawable.ic_account)))
                .build();

        //Nav List
        navigationDrawerLeft = new Drawer()
                .withActivity(this)
                .withToolbar(toolbar)
                .withDisplayBelowToolbar(false)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.LEFT)
                .withSavedInstance(savedInstanceState)
                .withAccountHeader(headerNavigationLeft)
                .withSelectedItem(0)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        switch (position) {
//                            case 0://home
////                                Intent a = new Intent(getApplicationContext(), MainActivity.class);
////                                startActivity(a);
//                                break;

                            case 0://kategori
                                Intent intent = new Intent(getApplicationContext(), AddActivity.class);
                                intent.putExtra("username", user);
                                startActivity(intent);
                                break;


                            case 1://tutup
                                SharedPreferences pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor = pref.edit();
                                editor.clear();
                                editor.commit();
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                break;
                        }
                    }
                })
                .build();

        // navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Home").withIcon(getResources().getDrawable(R.drawable.ic_home_black_24dp)));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Add Data").withIcon(getResources().getDrawable(R.drawable.ic_assignment_black_24dp)));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Logout").withIcon(getResources().getDrawable(R.drawable.ic_exit_to_app_black_24dp)));

    }

    private void showData(String jsonString) {
        List<ListData> data = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray result = jsonObject.getJSONArray(Config.TAG_JSON_ARRAY);

            for (int i = 0; i < result.length(); i++) {
                JSONObject jo = result.getJSONObject(i);
                ListData listData = new ListData();
                listData.id = jo.getString(Config.TAG_ID);
//                listData.air = jo.getString(Config.TAG_AIR);
                listData.kwh = jo.getString(Config.TAG_KWH);
                listData.bkr = jo.getString(Config.TAG_BKR);
                listData.pr = jo.getString(Config.TAG_PhaseR);
                listData.ps = jo.getString(Config.TAG_PhaseS);
                listData.pt = jo.getString(Config.TAG_PhaseT);
                listData.tgl = jo.getString(Config.TAG_DATE);
//                listData.user = jo.getString(Config.KEY_USER);
                data.add(listData);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        adapter = new RecycleAdapter(this, data);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

    }

    public void GetJSON(){
        class GetJSON extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                showData(s);
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequest(Config.URL_GET_ALL+user);
                return s;
            }
        }
        new GetJSON().execute();
    }
    private void searc() {
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setVoiceSearch(true);
        //searchView.setCursorDrawable(R.drawable.custom_cursor);
        searchView.setEllipsize(true);
        //searchView.setSuggestions(getResources().getStringArray(R.array.query_suggestions));
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Snackbar.make(findViewById(R.id.coorlayout), "Query : " + query, Snackbar.LENGTH_LONG)
                        .show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText.toString().trim());
                recyclerView.invalidate();
                return true;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (android.R.id.home == item.getItemId()) {
            new SplashScreen().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                    .setMessage("Are you sure you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).setNegativeButton("No", null).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
