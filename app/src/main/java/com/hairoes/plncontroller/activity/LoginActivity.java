package com.hairoes.plncontroller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.hairoes.plncontroller.Config;
import com.hairoes.plncontroller.R;
import com.hairoes.plncontroller.model.RequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {


    private EditText tusername;
    private EditText tuserpass;

    final String TAG = this.getClass().getName();
    private CheckBox cekRemember;
    SharedPreferences.Editor editor;
    private boolean checkFLag;
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tusername = (EditText) findViewById(R.id.editTextUserName);
        tuserpass = (EditText) findViewById(R.id.editTextPassword);
        cekRemember = (CheckBox) findViewById(R.id.cekRemember);


        Button login = (Button) findViewById(R.id.buttonLogin);
       // TextView create = (TextView) findViewById(R.id.buttonCreateAccount);

        cekRemember.setOnCheckedChangeListener(this);
        login.setOnClickListener(this);
    //    create.setOnClickListener(this);
        checkFLag = cekRemember.isChecked();
        Log.d(TAG, "checkFlag: " + checkFLag);

        pref = getSharedPreferences("login.conf", Context.MODE_PRIVATE);
        editor = pref.edit();

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.buttonLogin) {
            final String username = tusername.getText().toString();
            final String userpass = tuserpass.getText().toString();


            class getUser extends AsyncTask<Void, Void, String> {

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    Log.d(TAG, s);
                    String nama = null;
                    String user = null;
                    if (s.contains("result")) {
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            JSONArray result = jsonObject.getJSONArray("result");
                            JSONObject jo = result.getJSONObject(0);
                            nama =  jo.getString("nama");
                            user =  jo.getString("username");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(LoginActivity.this, "Login success !", Toast.LENGTH_LONG).show();
                        if (checkFLag) {
                            editor.putString("username", username);
                            editor.putString("password", userpass);
                            editor.putString("nama", nama);
                            editor.apply();
                        }
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("nama",nama);
                        intent.putExtra("username",user);
                        startActivity(intent);
                    }
                    else {
                       Toast.makeText(LoginActivity.this, "Login failed & try again !", Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                protected String doInBackground(Void... params) {
                    RequestHandler rh = new RequestHandler();
                    String s = rh.sendGetRequestUser(Config.URL_LOGIN,username,userpass);
                    return s;
                }
            }
            new getUser().execute();
        } else {
            //startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        checkFLag = isChecked;
        Log.d(TAG, "checkFlag: " + checkFLag);
    }
}
