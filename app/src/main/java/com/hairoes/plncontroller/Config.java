package com.hairoes.plncontroller;

/**
 * Created by hairoes on 29-Jul-16.
 */
public class Config {
    //Address of our scripts of the CRUD
    public static final String URL_SERVER = "http://192.168.56.1/pln-server/";
    public static final String URL_ADD = URL_SERVER + "add.php";
    public static final String URL_GET_ALL = URL_SERVER + "getall.php?username=";
    public static final String URL_GET_DATA = URL_SERVER + "get.php?id=";
    public static final String URL_UPDATE = URL_SERVER + "update.php";
    public static final String URL_DELETE = URL_SERVER + "delete.php?id=";
    public static final String URL_ADD_ACCOUNT = URL_SERVER + "adduser.php";
    public static final String URL_LOGIN = URL_SERVER + "login.php?";

    //Keys that will be used to send the request to php scripts
    public static final String KEY_ID = "id";
    public static final String KEY_AIR = "air";
    public static final String KEY_KWH = "kwh";
    public static final String KEY_BKR = "bahan_bakar";
    public static final String KEY_DATE = "tanggal";
    public static final String KEY_PhaseR = "phase_r";
    public static final String KEY_PhaseS = "phase_s";
    public static final String KEY_PhaseT = "phase_t";

    public static final String KEY_NAME = "nama";
    public static final String KEY_ADDR = "alamat";
    public static final String KEY_TELP = "telp";
    public static final String KEY_USER = "username";
    public static final String KEY_PASS = "password";

    //JSON Tags
    public static final String TAG_JSON_ARRAY = "result";
    public static final String TAG_ID = "id";
    public static final String TAG_AIR = "air";
    public static final String TAG_KWH = "kwh";
    public static final String TAG_BKR = "bahan_bakar";
    public static final String TAG_DATE = "tanggal";
    public static final String TAG_PhaseR = "phase_r";
    public static final String TAG_PhaseS = "phase_s";
    public static final String TAG_PhaseT = "phase_t";


}
