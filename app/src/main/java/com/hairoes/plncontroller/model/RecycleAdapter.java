package com.hairoes.plncontroller.model;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import com.hairoes.plncontroller.Config;
import com.hairoes.plncontroller.R;
import com.hairoes.plncontroller.activity.DetailActivity;
import com.hairoes.plncontroller.activity.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.zip.Inflater;

/**
 * Created by hairoes on 28-Jul-16.
 */
public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.ReciclerViewHolder> {

    List<ListData> ddata = Collections.emptyList();
    ArrayList<ListData> arrayList;
    private final Context context;
    private final LayoutInflater inflater;

    public RecycleAdapter(Context context, List<ListData> data) {
        this.context = context;
        this.ddata = data;
        inflater = LayoutInflater.from(context);
        arrayList = new ArrayList<ListData>();
        arrayList.addAll(ddata);
    }

    @Override
    public ReciclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = inflater.inflate(R.layout.custom_rows, parent, false);
        ReciclerViewHolder holder = new ReciclerViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ReciclerViewHolder holder, final int position) {
        ListData current = ddata.get(position);
        holder.txtTgl.setText(current.tgl);
        holder.txtKwh.setText(current.kwh + " KWH");
        holder.txtBkr.setText(current.bkr + " L");
    }

    @Override
    public int getItemCount() {
        return ddata.size();
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());

        ddata.clear();
        if (charText.length() == 0) {
            ddata.addAll(arrayList);

        } else {
            for (ListData postDetail : arrayList) {
                if (charText.length() != 0 && postDetail.getId().toLowerCase(Locale.getDefault()).contains(charText)) {
                    ddata.add(postDetail);
                } else if (charText.length() != 0 && postDetail.getTgl().toLowerCase(Locale.getDefault()).contains(charText)) {
                    ddata.add(postDetail);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class ReciclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private final TextView txtTgl, txtKwh, txtBkr;

        public ReciclerViewHolder(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            txtTgl = (TextView) itemView.findViewById(R.id.textViewTgl);
            txtKwh = (TextView) itemView.findViewById(R.id.textViewKwh);
            txtBkr = (TextView) itemView.findViewById(R.id.textViewBkr);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            ListData current = ddata.get(position);
            Intent intent = new Intent(context, DetailActivity.class);
            intent.putExtra(Config.KEY_ID, current.id);
          //  intent.putExtra(Config.KEY_AIR, current.air);
            intent.putExtra(Config.KEY_KWH, current.kwh);
            intent.putExtra(Config.KEY_BKR, current.bkr);
            intent.putExtra(Config.KEY_PhaseR, current.pr);
            intent.putExtra(Config.KEY_PhaseS, current.ps);
            intent.putExtra(Config.KEY_PhaseT, current.pt);
            intent.putExtra(Config.KEY_DATE, current.tgl);
            intent.putExtra(Config.KEY_USER, current.user);
            context.startActivity(intent);
        }

    }


}